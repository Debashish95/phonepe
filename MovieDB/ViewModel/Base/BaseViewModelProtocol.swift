//
//  BaseViewModelProtocol.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

public protocol BaseViewModelProtocol {
    func viewDidLoad()
}
