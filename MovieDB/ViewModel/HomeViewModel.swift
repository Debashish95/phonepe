//
//  HomeViewModel.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

public protocol HomeViewModelProtocol: BaseViewModelProtocol {
    var updateMovieList: (([MovieListModelProtocol]) -> ())? { get set }
}

public class HomeViewModel: HomeViewModelProtocol {
    
    private var movieListRe: [MovieListModelProtocol]?
    public var updateMovieList: (([MovieListModelProtocol]) -> ())?
    
    public func viewDidLoad() {
        
        let movieListRequest = getHomeAPIRequest(pageNumber: "1")
        
        APIManager.shared.getMovieList(request: movieListRequest) { response in
            DispatchQueue.main.async { [weak self] in
                if let result = response?.results {
                    let response = result.map({ MovieListModelMapper(movieItem: $0) })
                    self?.updateMovieList?(response)
                } else {
                    self?.updateMovieList?([])
                }
            }
        }
    }
    
    private func getHomeAPIRequest(pageNumber: String) -> MovieListRequest {
        var movieListRequest = MovieListRequest()
        movieListRequest.queryParameters = [
            "api_key": APIHelper.apiKey.rawValue,
            "language": APIHelper.language.rawValue,
            "page": pageNumber
        ]
        return movieListRequest
    }
}
