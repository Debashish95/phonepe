//
//  APIHelper.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

enum APIHelper: String {
    case apiKey = "38a73d59546aa378980a88b645f487fc"
    case language = "en-US"
}

/**

 Entity: PlayList
    i. Playlist name String
    ii. MovieId      String
 
 insert(Playlist, MovieId)
 1  "1,2,3,4,5,6,7,8,9"
 
 */
