//
//  APIRequestProtocol.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

public protocol APIRequestProtocol {
    var baseURL: String { get }
    var path: String { get }
    var queryParameters: [String: String]? { get }
    var httpMethod: String { get }
}
