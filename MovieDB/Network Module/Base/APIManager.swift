//
//  APIManager.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

public class APIManager {
    
    private init() {
        NSLog("APIManager Initialized")
    }
    
    public static let shared = APIManager()
    
    public func getMovieList(
        request: MovieListRequest,
        completion: @escaping (MovieListResponse?) -> ()
    ) {
        guard let baseURL = URL(string: request.baseURL) else {
            return completion(nil)
        }
        
        var urlComponent = URLComponents(string: baseURL.absoluteString)
        urlComponent?.path = request.path
        
        var queryItem: [URLQueryItem] =  []
        request.queryParameters?.forEach({ key, value in
            queryItem.append(URLQueryItem(name: key, value: value))
        })
        
        urlComponent?.queryItems = queryItem
        
        guard let finalURL = urlComponent?.url else {
            return
        }
        
        var urlRequest = URLRequest(url: finalURL)
        urlRequest.httpMethod = request.httpMethod
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            
            guard let data = data else {
                return completion(nil)
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                let jsonResult = try jsonDecoder.decode(MovieListResponse.self, from: data)
                completion(jsonResult)
            } catch {
                return completion(nil)
            }
            
            
        }.resume()
    }
}
