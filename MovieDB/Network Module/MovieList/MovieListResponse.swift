//
//  MovieListResponse.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation
public struct MovieListResponse: Codable {
    let page: Int?
    let results: [MovieList]?
    let totalPages, totalResults: Int?
    let statusMessage: String?
    let statusCode: Int?
}

public struct MovieList: Codable {
    let adult: Bool?
    let backdropPath: String?
    let genreIDS: [Int]?
    let id: Int?
    let originalLanguage, originalTitle, overview: String?
    let popularity: Double?
    let posterPath, releaseDate, title: String?
    let video: Bool?
    let voteAverage: Double?
    let voteCount: Int?

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case genreIDS = "genre_ids"
        case id
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}
