//
//  MovieListRequest.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

public struct MovieListRequest: APIRequestProtocol {
    public var baseURL: String {
        "https://api.themoviedb.org"
    }
    public var path: String {
        "/3/movie/popular"
    }
    public var queryParameters: [String : String]?
    
    public var httpMethod: String {
        "GET"
    }
}
