//
//  MovieListModelMapper.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import Foundation

public protocol MovieListModelProtocol {
    var image: String? { get }
}

public struct MovieListModelMapper: MovieListModelProtocol {
    public var image: String? {
        if let backDropPath = movieItem.backdropPath {
            return "https://image.tmdb.org/t/p/w500" + backDropPath
        }
        return nil
    }
    
    private var movieItem: MovieList
    public init(movieItem: MovieList) {
        self.movieItem = movieItem
    }
}
