//
//  HomeTableViewCell.swift
//  MovieDB
//
//  Created by Debashish Dash on 26/12/22.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var playListName: UILabel!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var data: MovieListModelProtocol? {
        didSet {
            if let data = data {
                bannerImageView.loadImage(from: data.image ?? "")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
