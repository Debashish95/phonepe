//
//  HomeViewController.swift
//  MovieDB
//
//  Created by Mohit Jethwa on 08/03/22.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var homeViewModel: HomeViewModel?
    
    var movieListResult: [MovieListModelProtocol]? {
        didSet {
            tableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        setupUI()
    }
    
    private func setupViewModel() {
        homeViewModel = HomeViewModel()
        homeViewModel?.updateMovieList = { [weak self] movieList in
            self?.movieListResult = movieList
        }
        
        homeViewModel?.viewDidLoad()
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
    }
}

